-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 01, 2012 at 05:45 AM
-- Server version: 5.1.49
-- PHP Version: 5.3.3-7+squeeze8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `survey`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `question` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  UNIQUE KEY `question` (`question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`question`, `image`) VALUES
(28, 'bodyshapes.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `email` varchar(100) NOT NULL,
  `question` int(11) NOT NULL,
  `most` int(11) NOT NULL,
  `least` char(11) NOT NULL,
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `M` int(1) DEFAULT NULL,
  `L` int(1) DEFAULT NULL,
  `Mstyle` varchar(100) NOT NULL DEFAULT '',
  `Lstyle` varchar(100) NOT NULL DEFAULT '',
  `question` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `priority` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`answer_id`, `M`, `L`, `Mstyle`, `Lstyle`, `question`, `answer`, `priority`) VALUES
(1, 3, 0, 'S', 'S', 1, 'Gentle, Kindly', '3'),
(2, 2, 1, 'I', 'N', 1, 'Persuasive, convinces others', '3'),
(3, 1, 2, 'C', 'C', 1, 'Humble, reserved, modest', '3'),
(4, 0, 3, 'N', 'D', 1, 'Original, inventive, individualistic', '3'),
(5, 2, 0, 'I', 'I', 2, 'Attractive, charming, attracts others', '3'),
(6, 3, 2, 'C', 'C', 2, 'Cooperative, agreeable', '3'),
(7, 0, 3, 'D', 'D', 2, 'Stubborn, unyielding', '3'),
(8, 1, 1, 'N', 'S', 2, 'Sweet, pleasing', '3'),
(9, 1, 2, 'N', 'C', 3, 'Easily led, follower', '3'),
(10, 0, 3, 'D', 'D', 3, 'Bold, daring', '3'),
(11, 3, 0, 'S', 'N', 3, 'Loyal and devoted', '3'),
(12, 2, 1, 'I', 'I', 3, 'Charming, delightful', '3'),
(13, 3, 2, 'C', 'N', 4, 'Open-minded, receptive to ideas of others', '3'),
(14, 1, 1, 'S', 'S', 4, 'Obliging, helpful', '3'),
(15, 0, 3, 'N', 'D', 4, 'Willpower, strong willed', '3'),
(16, 2, 0, 'I', 'I', 4, 'Cheerful, joyful', '3'),
(17, 3, 0, 'N', 'I', 5, 'Jovial, joking', '3'),
(18, 1, 1, 'C', 'C', 5, 'Precise, exact', '3'),
(19, 0, 3, 'N', 'D', 5, 'Nervy, gutsy, brazen', '3'),
(20, 2, 2, 'S', 'S', 5, 'Even-tempered, calm, not easily excited', '3'),
(21, 0, 3, 'D', 'D', 6, 'Competitive, seeking to win', '3'),
(22, 3, 1, 'S', 'S', 6, 'Considerate, caring, thoughtful', '3'),
(23, 2, 0, 'N', 'I', 6, 'Outgoing, fun loving, socially striving', '3'),
(24, 1, 2, 'N', 'C', 6, 'Harmonious, agreeable', '3'),
(25, 1, 3, 'N', 'C', 7, 'Fussy, hard to please', '3'),
(26, 2, 1, 'S', 'N', 7, 'Obedient, will do as told, dutiful', '3'),
(27, 0, 2, 'D', 'D', 7, 'Unconquerable, determined', '3'),
(28, 3, 0, 'I', 'I', 7, 'Playful, frisky, full of fun', '3'),
(29, 0, 2, 'D', 'N', 8, 'Brave, unafraid, courageous', '3'),
(30, 3, 0, 'I', 'N', 8, 'Inspiring, stimulating, motivating', '3'),
(31, 2, 1, 'N', 'S', 8, 'Submissive, yielding, gives in', '3'),
(32, 1, 3, 'N', 'C', 8, 'Timid, shy, quiet', '3'),
(33, 3, 1, 'I', 'I', 9, 'Sociable, enjoys company of others', '3'),
(34, 2, 0, 'S', 'S', 9, 'Patient, steady, tolerant', '3'),
(35, 0, 3, 'D', 'D', 9, 'Self-reliant, independent', '3'),
(36, 1, 2, 'C', 'N', 9, 'Soft-spoken, mild, reserved', '3'),
(37, 2, 0, 'D', 'D', 10, 'Adventurous, willing to take chances', '3'),
(38, 3, 1, 'C', 'N', 10, 'Receptive, open to suggestions', '3'),
(39, 1, 2, 'N', 'I', 10, 'Cordial, warm, friendly', '3'),
(40, 0, 3, 'S', 'S', 10, 'Moderate, avoids extremes', '3'),
(41, 3, 0, 'I', 'I', 11, 'Talkative, chatty', '3'),
(42, 1, 2, 'S', 'S', 11, 'Controlled, restrained', '3'),
(43, 2, 1, 'N', 'C', 11, 'Conventional, doing it the usual way, customary', '3'),
(44, 0, 3, 'D', 'D', 11, 'Decisive, certain, firm in making a decision', '3'),
(45, 1, 1, 'N', 'I', 12, 'Polished, smooth talker', '3'),
(46, 0, 3, 'D', 'D', 12, 'Daring, risk-taker', '3'),
(47, 2, 2, 'C', 'N', 12, 'Diplomatic, tactful to people', '3'),
(48, 3, 0, 'S', 'S', 12, 'Satisfied, content, pleased', '3'),
(49, 0, 1, 'D', 'N', 13, 'Aggressive, challenger, takes action', '3'),
(50, 3, 0, 'I', 'I', 13, 'Center of attention, outgoing, entertaining', '3'),
(51, 2, 2, 'S', 'S', 13, 'Easy mark, easily taken advantage of', '3'),
(52, 1, 3, 'N', 'C', 13, 'Fearful, afraid', '3'),
(53, 1, 3, 'C', 'C', 14, 'Cautious, wary, careful', '3'),
(54, 0, 2, 'D', 'N', 14, 'Determined, decided, unwavering, firm', '3'),
(55, 2, 0, 'I', 'I', 14, 'Convincing, assuring', '3'),
(56, 3, 1, 'S', 'N', 14, 'Good-natured, pleasant', '3'),
(57, 3, 0, 'S', 'N', 15, 'Willing, goes along with', '3'),
(58, 0, 2, 'N', 'N', 15, 'Eager, anxious', '3'),
(59, 2, 1, 'C', 'C', 15, 'Agreeable, consenting', '3'),
(60, 1, 3, 'N', 'D', 15, 'High-spirited, lively, enthusiastic', '3'),
(61, 3, 2, 'I', 'N', 16, 'Confident, believes in self, assured', '3'),
(62, 1, 1, 'N', 'S', 16, 'Sympathetic, compassionate, understanding', '3'),
(63, 2, 0, 'N', 'C', 16, 'Tolerant', '3'),
(64, 0, 3, 'D', 'D', 16, 'Assertive, aggressive', '3'),
(65, 1, 1, 'C', 'N', 17, 'Well-disciplined, self-controlled', '3'),
(66, 3, 0, 'S', 'S', 17, 'Generous, willing to share', '3'),
(67, 2, 2, 'N', 'I', 17, 'Animated, uses gestures for expression', '3'),
(68, 0, 3, 'D', 'D', 17, 'Persistent, unrelenting, refuses to quit', '3'),
(69, 3, 1, 'I', 'N', 18, 'Admirable, deserving of praise', '3'),
(70, 2, 0, 'S', 'N', 18, 'Kind, willing to help, giving', '3'),
(71, 1, 2, 'N', 'C', 18, 'Resigned, gives in', '3'),
(72, 0, 3, 'D', 'D', 18, 'Force of character, powerful', '3'),
(73, 1, 2, 'C', 'N', 19, 'Respectful, shows respect', '3'),
(74, 0, 3, 'D', 'D', 19, 'Pioneering, exploring, enterprising', '3'),
(75, 3, 0, 'I', 'I', 19, 'Optimistic, positive', '3'),
(76, 2, 1, 'S', 'S', 19, 'Accommodating, willing to please, helpful', '3'),
(77, 0, 3, 'D', 'D', 20, 'Argumentative, confronting', '3'),
(78, 2, 0, 'C', 'N', 20, 'Adaptable, flexible', '3'),
(79, 1, 2, 'N', 'S', 20, 'Nonchalant, casually indifferent', '3'),
(80, 3, 1, 'I', 'I', 20, 'Lighthearted, carefree', '3'),
(81, 1, 2, 'S', 'I', 21, 'Trusting, faith in others', '3'),
(82, 0, 3, 'N', 'S', 21, 'Contented, satisfied', '3'),
(83, 3, 0, 'D', 'D', 21, 'Positive, admitting no doubt', '3'),
(84, 2, 1, 'C', 'C', 21, 'Peaceful, tranquil', '3'),
(85, 2, 2, 'I', 'I', 22, 'Good mixer, likes being with others', '3'),
(86, 1, 3, 'N', 'C', 22, 'Cultured, educated, knowledgeable', '3'),
(87, 0, 0, 'D', 'D', 22, 'Vigorous, energetic', '3'),
(88, 3, 1, 'S', 'S', 22, 'Lenient, not strict, tolerant', '3'),
(89, 3, 0, 'I', 'I', 23, 'Companionable, easy to be with', '3'),
(90, 2, 1, 'C', 'N', 23, 'Accurate, correct', '3'),
(91, 0, 2, 'D', 'D', 23, 'Outspoken, speaks freely and boldly', '3'),
(92, 1, 3, 'N', 'S', 23, 'Restrained, reserved, controlled', '3'),
(93, 1, 3, 'D', 'D', 24, 'Restless, unable to rest of relax', '3'),
(94, 2, 1, 'S', 'S', 24, 'Neighborly, friendly', '3'),
(95, 0, 2, 'I', 'I', 24, 'Popular, liked by many or most people', '3'),
(96, 3, 0, 'C', 'C', 24, 'Orderly, neat, organized', '3'),
(97, 3, 0, '', '', 25, 'Cancer, Capricorn,  Pieces', '2'),
(98, 2, 1, '', '', 25, 'Taurus, Gemini, Scorpio', '2'),
(99, 1, 2, '', '', 25, 'Libra, Aquarius, Aries', '2'),
(100, 0, 3, '', '', 25, 'Leo, Virgo, Sagittarius', '2'),
(101, 3, 0, '', '', 26, 'Libertarian, individual liberty, free trade', '1'),
(102, 0, 3, '', '', 26, 'Democrat, liberal socialist, equality', '1'),
(103, 1, 2, '', '', 26, 'Republican, conservative, big business', '1'),
(104, 2, 1, '', '', 26, 'Centrist, independent, other', '1'),
(105, 0, 2, '', '', 27, '<a href=size.htm target=_blank>The color of my size</a> is Pink', '1'),
(106, 1, 1, '', '', 27, '<a href=size.htm target=_blank>The color of my size</a> is Yellow', '1'),
(107, 3, 0, '', '', 27, ' <a href=size.htm target=_blank>The color of my size</a> is Orange', '1'),
(108, 0, 3, '', '', 27, '<a href=size.htm target=_blank>The color of my size</a> is Blue', '1'),
(109, 3, 0, '', '', 28, 'I have an hourglass shaped (mesomorph) body type.', '1'),
(110, 1, 2, '', '', 28, 'I have a pear shaped (endomorph) body type.', '1'),
(111, 0, 3, '', '', 28, 'I have an apple shaped (mesomorphs / endomorphs) body type.', '1'),
(112, 2, 1, '', '', 28, 'I have a ruler shaped (ectomorph) body type.', '1'),
(113, 0, 3, '', '', 29, 'I have HIV / AIDs', '1'),
(114, 3, 0, '', '', 29, 'I have genital herpes (HPV2)', '1'),
(115, 2, 1, '', '', 29, 'I have cold sores (HPV1) or a curable STD (like gonorrhea)', '1'),
(116, 1, 2, '', '', 29, 'I do NOT have any sexually transmitted diseases', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
