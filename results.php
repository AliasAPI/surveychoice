<?

// Connect
require_once("private/config.php");

// Get the email address from the address bar.
$email = $_GET['email'];
// $email = 'test@test.com';

// If the email is set
if( $email )
{
  // Make sure the user's email address is in the results table
  list( $email_found ) = mysql_fetch_row(mysql_query("SELECT email FROM results
    WHERE email = '" . $email . "' "));

  // If the user's email address is in the results table
  if( $email_found )
  {
    // Get all of the priorities
    $priorities = mysql_query("SELECT priority FROM survey
      GROUP BY priority ");

    // Loop through the priorities
    while( list( $priority ) = mysql_fetch_array($priorities))
    {
      // Get all of the questions that have been answered
      $answered_questions = mysql_query("SELECT S.question as Q
        FROM survey AS S
        INNER JOIN results AS R
        ON S.question = R.question
        AND email = '" . $email . "'
        AND S.question IS NOT NULL
        AND S.priority = '" . $priority . "'
        GROUP BY S.question ");

      // Loop through the answered questions
      while( list( $question ) = mysql_fetch_array($answered_questions))
      {
        // Get the answer ids for the question
        list( $m_answer_id, $l_answer_id ) = mysql_fetch_row(mysql_query("SELECT most, least FROM results
          WHERE question = '" . $question . "'
          AND email = '" . $email . "' "));

        // Select the M attributes
        list( $Manswer, $Mpoints, $Mstyle ) = mysql_fetch_row(mysql_query("SELECT answer,M,Mstyle FROM survey
          WHERE answer_id = '" . $m_answer_id . "' "));

        // Select the L attributes
        list( $Lanswer, $Lpoints, $Lstyle ) = mysql_fetch_row(mysql_query("SELECT answer,L,Lstyle FROM survey
          WHERE answer_id = '" . $l_answer_id . "' "));

        $results_0_rows .= "<tr>
          <td>$Manswer</td>
          <td> NOT </td>
          <td>$Lanswer</td>
          <td align=center>$Mstyle</td>
          <td align=center>$Lstyle</td>
          <td align=right>$Mpoints</td>
          <td align=right>$Lpoints</td></tr>";
      }


      // Get the total points for each priority based on the user's answers
      // . . . for max
      list( $priority_M_score ) = mysql_fetch_row(mysql_query("SELECT SUM(M) AS Total
        FROM ( SELECT M as M
        FROM survey AS S
        INNER JOIN results AS R
        ON S.question = R.question
        AND email = '" . $email . "'
        AND S.question IS NOT NULL
        AND S.priority = '" . $priority . "'
        GROUP BY S.question) AS T "));

      // . . . for least
      list( $priority_L_score ) = mysql_fetch_row(mysql_query("SELECT SUM(L) AS Total
        FROM ( SELECT L as L
        FROM survey AS S
        INNER JOIN results AS R
        ON S.question = R.question
        AND email = '" . $email . "'
        AND S.question IS NOT NULL
        AND S.priority = '" . $priority . "'
        GROUP BY S.question) AS T "));

      // Get the max points for each priority grouping based on the questions that have been answered
      //  . . . for max
      list( $priority_M_total ) = mysql_fetch_row(mysql_query("SELECT SUM(MOST) AS Total
        FROM ( SELECT MAX(M) as MOST
        FROM survey AS S
        INNER JOIN results AS R
        ON S.question = R.question
        AND email = '" . $email . "'
        AND S.question IS NOT NULL
        AND S.priority = '" . $priority . "'
        GROUP BY S.question) AS T "));

      // . . . for least
      list( $priority_L_total ) = mysql_fetch_row(mysql_query("SELECT SUM(LEAST) AS Total
        FROM ( SELECT MAX(L) as LEAST
        FROM survey AS S
        INNER JOIN results AS R
        ON S.question = R.question
        AND email = '" . $email . "'
        AND S.question IS NOT NULL
        AND S.priority = '" . $priority . "'
        GROUP BY S.question) AS T "));

      // Combine the score the user earned
      $p_score = $priority_M_score + $priority_L_score;

      // Combine the totals for the priority
      $p_total = $priority_M_total + $priority_L_total;

      // Calculate the percent
      $p_percent = round((($p_score / $p_total) * 100), 2);

      // Append stats for each priority
      $results_1 .= "Group [ $priority ] Points [ $p_score ] / Possible [ $p_total ] = [ $p_percent% ]<br>\n";

      // Calculate the overall stats
      $p_score_total = $p_score_total + $p_score;
      $p_total_total = $p_total_total + $p_total;

      $p_percent_total = round((($p_score_total / $p_total_total) * 100), 2);
    }

    $results_0_header = "<table cellspacing=5><tr>
      <td><b>MOST<b></td>
      <td> </td>
      <td><b>LEAST</b></td>
      <td>MOST</td>
      <td>LEAST</td>
      <td>M</td>
      <td>L</td></tr>";
    $results_0_footer = "</table>";

    $results_0 = "$results_0_header $results_0_rows $results_0_footer";

    $results_2 = "Total Points [ $p_score_total ] / Total Possible [ $p_total_total ] = Overall [ $p_percent_total ] ";

    // Tally the Most Personality style points
    list( $M_points, $M_D, $M_I, $M_S, $M_C, $M_N ) = mysql_fetch_row(mysql_query("SELECT
      SUM(M) AS Mpoints,
        SUM(IF(S.Mstyle = 'D', 1, 0)) AS M_D,
        SUM(IF(S.Mstyle = 'I', 1, 0)) AS M_I,
        SUM(IF(S.Mstyle = 'S', 1, 0)) AS M_S,
        SUM(IF(S.Mstyle = 'C', 1, 0)) AS M_C,
        SUM(IF(S.Mstyle = 'N', 1, 0)) AS M_N
        FROM survey AS S
        LEFT JOIN results AS R
        ON S.question = R.question
        WHERE S.answer_id = R.most
        AND R.email = '" . $email . "'
        AND S.priority = '3' "));

    // Tally the Least Personality style points
    list( $L_points, $L_D, $L_I, $L_S, $L_C, $L_N ) = mysql_fetch_row(mysql_query("SELECT
      SUM(L) AS Lpoints,
        SUM(IF(S.Lstyle = 'D', 1, 0)) AS L_D,
        SUM(IF(S.Lstyle = 'I', 1, 0)) AS L_I,
        SUM(IF(S.Lstyle = 'S', 1, 0)) AS L_S,
        SUM(IF(S.Lstyle = 'C', 1, 0)) AS L_C,
        SUM(IF(S.Lstyle = 'N', 1, 0)) AS L_N
        FROM survey AS S
        LEFT JOIN results AS R
        ON S.question = R.question
        WHERE S.answer_id = R.least
        AND R.email = '" . $email . "'
        AND S.priority = '3' "));

    // Most DISCN = Total Max Personality Points
    $results_3 = "<CENTER><TABLE border=0 cellpadding=9 cellspacing=3>
      <TR><TD>MOST:
      <td align=center><font size=+1>D</font></td><td align=center>$M_D</td>
      <td align=center><font size=+1>I</font></td><td align=center>$M_I</td>
      <td align=center><font size=+1>S</font></td><td align=center>$M_S</td>
      <td align=center><font size=+1>C</font></td><td align=center>$M_C</td>
      <td align=center><font size=+1>N</font></td><td align=center>$M_N</td></tr> ";

    // Least DISCN = Total Least Personality Points
    $results_3 .= "
      <TR><TD>LEAST:
      <td align=center><font size=+1>D</font></td><td align=center>$L_D</td>
      <td align=center><font size=+1>I</font></td><td align=center>$L_I</td>
      <td align=center><font size=+1>S</font></td><td align=center>$L_S</td>
      <td align=center><font size=+1>C</font></td><td align=center>$L_C</td>
      <td align=center><font size=+1>N</font></td><td align=center>$L_N</td>
      </TABLE></CENTER>";

    $test_results = "<center><font size=+2>Results for $email</font><br><br>\n $results_0 <br>Personality Profile: $results_3 $results_1 $results_2</center>";

    echo "<p><br></p>";
    echo "<center><font size=+3>Finished.<br><font color=green>Thank you!</font></font><br><br>";
    echo $test_results;


    $from = "drewbrownjr@gmail.com";
    $headers  = "From: $email\r\n";
    $headers .= "Content-type: text/html\r\n";

    // Email the results
    mail("drewbrownjr@gmail.com","SurveyChoice results for $email", $test_results, $headers);
  }
  else
  {
    echo "<br><br><br><center>The email [ $email ] is not found in the database.";
  }
}

?>
