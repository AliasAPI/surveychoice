<?

require_once("private/config.php");

// Only allow those who enter a valid email to take the survey
if(!isset($_GET['email']))
{
  echo "<center><br><br><font size=+3 color=red>No email address has been set.</font></center>";
  die();
}
else
{
  $email = $_GET['email'];
}

$question = "";
$most = "";
$least = "";

if (isset($_POST['submit']))
{
  $email = $_POST['email'];
  $question = $_POST['question'];
  $m_answer_id = $_POST['most'];
  $l_answer_id = $_POST['least'];

  // Delete any previous answers to the question
  mysql_query("DELETE FROM results WHERE email = '" . $email . "' AND question = '" . $question . "' ");

  // Record the new answer
  // echo "INSERT INTO results (`email`, `question`, `most`, `least`) VALUES ('" . $email . "', '" . $question . "', '" . $m_answer_id . "', '" . $l_answer_id . "') ";
  mysql_query("INSERT INTO results (`email`, `question`, `most`, `least`)
    VALUES ('" . $email . "', '" . $question . "', '" . $m_answer_id . "', '" . $l_answer_id . "') ");
}

// Delete all answers for the user if the RESET button is pressed
if (isset($_POST['reset']))
{
  mysql_query("DELETE FROM results WHERE email = '" . $email . "' ");
}

// Count all questions
$result = mysql_query("SELECT question FROM survey GROUP BY question");
if ($result)
  $count = mysql_num_rows($result);

// Count all answers
list( $answer_count ) = mysql_fetch_row(mysql_query("SELECT COUNT(question) FROM results WHERE email = '$email' "));

// Number the questions so that the user knows the progress
$answer_count = ( $answer_count < $count ) ? $answer_count+1 : $answer_count;

// Select the questions that have not been answered
$result = mysql_query("SELECT survey.question FROM `survey` " .
  // where the survey question
  "WHERE survey.question " .
  // has not been answered in the results table
  "NOT IN (SELECT results.question FROM `results` " .
  // for the current user.
  "WHERE results.email = '" . $email . "') " .
  // Remove the duplicate questions and
  "GROUP BY survey.question " .
  // put the questions in order of group priorities, and then randomize the questions in the groups
  "ORDER BY survey.priority, RAND() ");

?>

<?
if ($result && ($row = mysql_fetch_row($result)))
{
  $question = $row[0];
?>



<html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>Survey</title>
<script language="javascript" type="text/javascript" src="jquery.js"></script>
<script language='Javascript'>
function checksub(form)
{
  var most=false, least=false;
  var tmost, tleast;
  for (i=0; i<form.most.length; i++)
  {
    most  |= form.most[i].checked;
    least |= form.least[i].checked;

    if (form.most[i].checked && form.least[i].checked)
    {
      alert("You cannot choose the same item as the MOST and the LEAST!");
      return false;
    }
  }

  if (!most)
  {
    alert("Please choose the MOST value!");
    return false;
  }
  if (!least)
  {
    alert("Please choose the LEAST value!");
    return false;
  }
  window.location.href = "index.php";
  return true;
}

function resetpage()
{
  window.location.href = "index.php";
  return true;
}
</script>
</head>
<body>
<p align=right><table border=0 align=right><tr><td><font face=arial size=-1>
    1. Please choose the selection that most describes you (or the most correct statement).<br/>
    2. Please choose the selection that least describes you (or the least correct statement).</font><br/><br/>
    <center><font face=arial size=-1><b>Question <?echo($answer_count)?> of <?echo($count)?></b></font></center><br/>
    </table></p>

<form method=post onSubmit='return checksub(this);'>
<input type=hidden name=email value="<? echo $email;?>">
<input type=hidden name=question value="<? echo $question;?>">
<center><p><br><br></p>

<?

  // Get the image display for the question
  list( $image ) = mysql_fetch_row(mysql_query("SELECT image FROM `images` WHERE question = '" . $question . "' "));

  // If there is an image for the question
  if( $image )
  {
    echo "<center><img src=image/$image border=0></center>";
  }
  else
  {
    echo "<p><br><br><br><br><br><br></p>";
  }

?>


<table width=500 border=0><tr><td align=center width=25><font face=arial size=-1><b>Most</b></font></td><td align=center width=20><font face=arial size=-1><b>Least</b></font></td><td align=right>
<?
  // Show answers
  $result1 = mysql_query("SELECT * FROM survey WHERE question = '" . $question . "' ORDER BY RAND()");

  while ($row1 = mysql_fetch_array($result1))
  {
    echo "<tr><td align=center width=20><input type=radio name=most value='" . $row1['answer_id'] . "'></td>
      <td align=center width=20><input type=radio name=least value='" . $row1['answer_id'] . "'></td>
      <td><font face=arial>" . $row1['answer'] . "</font></td>";
  }
  // Close table, form, etc
  echo "</tr><tr><td align=center colspan=2><br><input type=submit name='submit' value='Next'></form></td></tr></table></center>";
}
else
{
  // No unanswered questions == finished.

  // Redirect to the answer page
  echo "<meta http-equiv=\"Refresh\" content=\"0; url=results.php?email=$email\">";
}

?>
</body>
</html>
